<?php 
	$host = "localhost"; //where your database is located;
	$db_username = "root"; //username for the host;
	$db_password = "";  //password for the host;
	$db_name = "b55Ecommerce"; //database's name;

	// to create the connection;
	$conn = mysqli_connect($host, $db_username, $db_password, $db_name);
	// to check the connection
	if(!$conn) {
		die("Connection Failed: " . mysqli_error($conn));
	}
	?>