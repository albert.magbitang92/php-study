<?php
require "../Partials/template.php";

function get_title(){
	echo "History Page";
}

function get_body_contents(){
	require "../Controllers/connection.php";
	?>
	<h1 class="text-center py-5">History Page</h1>
	<div class="col-lg-8 offset-lg-2">
		<table class="table table-striped">
			<thead>
				<th>Order Id</th>
				<th>Product Image</th>
				<th>Total</th>
				<th>User Id</th>
				<th>Order Details</th>
				<th>Payment</th>
				<th>Status</th>
			</thead>
			<tbody>
				<?php
				$history_query = "SELECT id, total, user_id, status_id, payment_id FROM item_order JOIN items ON (items.id = item_order.item_id) JOIN orders ON (orders.id = item_order.order_id)";
				$history = mysqli_query($conn, $history_query);
				foreach ($history as $indiv_history) {
					?>
					<tr>
						<td><?= $indiv_history['id']?></td>
						<td><img src="<?= $indiv_history['imgPath']?>" height="100px"></td>
						<td><?= $indiv_history['name']?></td>
						<td><?= $indiv_history['total']?></td>
					</tr>
					<?php
				}
				?>
			</tbody>
		</table>
	</div>
	<?php
}

?>