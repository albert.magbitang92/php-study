<?php

require "../Partials/template.php";

function get_title(){
	echo "Catalog";
}

function get_body_contents(){
	// establish connection
	require "../Controllers/connection.php";
	?>
	<h1 class="text-center py-5">New Arrivals!</h1>
	<!-- ITEM LIST -->
	<div class="row">
		<?php 
			// publish items
		$items_query = "SELECT * FROM items";
		$items = mysqli_query($conn, $items_query);
		foreach ($items as $indiv_item){
			?>
			<div class="col-lg-4 py-2">
				<div class="card">
					<img class="card-img-top" height="200px" src="<?php echo $indiv_item['imgPath']?>" alt="">
					<div class="card-body">
						<h4 class="card-title">
							<?= $indiv_item['name']?>
						</h4>
						<p class="card-text">Price: Php <?=$indiv_item['price']?></p>
						<p class="card-text">Description: <?=$indiv_item['description']?></p>
						<p class="card-text">Category: 
						<?php 
						  $catId = $indiv_item['category_id'];
						  $category_query = "SELECT * FROM categories WHERE id = $catId";
						  $category = mysqli_fetch_assoc(mysqli_query($conn, $category_query));

						  if($category['id'] = $catId){
						  	echo $category['name'];}
						?>
						</p>
					</div>
					<div class="card-footer">
						<a href="../Controllers/delete-item-process.php?id=<?php echo $indiv_item['id']?>" class="btn btn-danger">Delete Item</a>
						<a href="edit-item.php?id=<?php echo $indiv_item['id']?>" class="btn btn-info">Edit Item</a>
					</div>
					<div class="card-footer">
						<!-- <form action="../Controllers/add-to-cart-process.php" method="POST"> -->
							<!-- <input type="hidden" name="id" value="<?php echo $indiv_item['id']?>"> -->
							<input type="number" name="cart" class="form-control" value="1">
							<button type="button" class="btn btn-success addToCart" data-id="<?php echo $indiv_item['id']?>">Add to Cart</button>
						<!-- </form> -->
					</div>
				</div>
			</div>
			<?php
		}
		?>
	</div>
	<script type="text/javascript" src="../Assets/Scripts/add-to-cart.js"></script>
	<?php
}
?>