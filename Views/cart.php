<?php 

require "../Partials/template.php";

function get_title(){
	echo "Cart";
}

function get_body_contents(){
	require "../Controllers/connection.php";

?>
<h1 class="text-center py-5">Cart Page</h1>
<hr>
<div class="col-lg-10 offset-lg-1">
	<table class="table table-striped table-bordered">
		<thead>
			<tr class="text-center">
				<th>Item:</th>
				<th>Price:</th>
				<th>Quantity:</th>
				<th>Subtotal:</th>
				<th></th>
			</tr>
		</thead>
		<tbody>
			<?php 
			$total = 0;
			if(isset($_SESSION['cart'])){
				foreach($_SESSION['cart'] as $itemId => $quantity){
					$item_query = "SELECT * FROM items WHERE id = $itemId";
					$indiv_item = mysqli_fetch_assoc(mysqli_query($conn, $item_query));
					$subtotal = $indiv_item['price']*$quantity;
					$total += $subtotal;
					?>
					<tr>
						<td>
							<?php echo $indiv_item['name']?>
						</td>
						<td>
							<?php echo $indiv_item['price']?>
						</td>
						<td>
							<span class="spanQ"><?php echo $quantity ?></span>
							<form action="../Controllers/add-to-cart-process.php" method="POST" class="d-none">
								<input type="hidden" name="id" value="<?php echo $itemId?>">
								<input type="hidden" name="fromCartPage" value="fromCartPage">
								<input type="number" class="form-control" value="<?php echo $quantity ?>" data-id ="<?php echo $itemId?>" name="cart">	
							</form>
						</td>
						<td>
							<?php echo number_format($subtotal, 2) ?>
						</td>	
						<td>
							<a href="../Controllers/remove-from-cart-process.php?id=<?php echo $itemId ?>" class="btn btn-danger">Remove Item</a>
						</td>
						<td></td>
					</tr>
					<?php
				}
			}
			?>		
			<tr>
				<td></td>
				<td></td>
				<td>Total: </td>
				<td id="totalPayment"><?php echo number_format($total, 2) ?></td>
				<td><a href="../Controllers/empty-cart-process.php" class="btn btn-danger" type="submit">Empty Cart</td>
			</tr>
			<tr>
				<td></td>
				<td></td>
				<td>
					<form action="../Controllers/checkout-process.php" method="POST">
						<input type="hidden" name="totalPayment" value="<?php echo $total?>">
						<input type="hidden" name="COD" value="total">
						<button type="submit" class="btn btn-info">Pay via Cash</button>
					</form>
				</td>
				<td>
					<div id="paypal-button-container"></div>
				</td>
				<td></td>
			</tr>
		</tbody>
	</table>
</div>
<script type="text/javascript" src="../Assets/Scripts/update-cart.js"></script>
<script src="https://www.paypal.com/sdk/js?client-id=ARMS-bdkLVGriX1Sm3k1Vpc41Vc12jhoA2vkSo9NUOwUgW7TxnpH4q1MidEMoaFHsWVymMLh513LIeTD"> // Required. Replace SB_CLIENT_ID with your sandbox client ID. </script>
<script>
let totalPayment = document.getElementById('totalPayment').textContent.split('.', ',').join("");
    paypal.Buttons({
    createOrder: function(data, actions) {
      return actions.order.create({
        purchase_units: [{
          amount: {
            value:totalPayment
          }
        }]
      });
    },
    onApprove: function(data, actions) {
      return actions.order.capture().then(function(details){
      	let data = new FormData;
      	data.append('totalPayment', totalPayment);
      	data.append('fromPaypal', fromPaypal);

      	fetch("../Controllers/checkout-process.php", {
      		method:"POST",
      		body:data
      	}).then(res=>res.text())
      	.then(res=>{
      		console.log(res);
      		alert('Transaction completed by ' + details.payer.name.given_name);	
      	})
        // Call your server to save the transaction
        return fetch('/paypal-transaction-complete', {
          method: 'post',
          headers: {
            'content-type': 'application/json'
          },
          body: JSON.stringify({
            orderID: data.orderID
          })
        });
      });
    }
  	}).render('#paypal-button-container');
</script>
<?php
}
?>






















