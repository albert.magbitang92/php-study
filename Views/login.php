<?php 

	require "../Partials/template.php";
	function get_title(){
		echo "Log In";
	}
	function get_body_contents(){

?>
	<h1 class="text-center py-5">Log In</h1>
	<div class="col-lg-8 offset-lg-2">
		<form action="" method="POST">
			<div class="form-group">
				<label for="email">E-Mail:</label>
				<input type="text" name="email" class="form-control" id="email">
				<span class="validation"></span>
			</div>
			<div class="form-group">
				<label for="password">Password:</label>
				<input type="password" name="password" class="form-control" id="password">
				<span class="validation"></span>
			</div>
			<button type="button" class="btn btn-success" id="loginUser">Sign In</button>
			<p>Not Registered Yet?</p>
			<a href="register.php"><p>Register</p></a>
		</form>
	</div>
	<script type="text/javascript" src="../Assets/Scripts/login.js"></script>
<?php
	}

?>