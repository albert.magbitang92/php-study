<?php 

require "../Partials/template.php";

function get_title(){
	echo "Cart";
}

function get_body_contents(){
	require "../Controllers/connection.php";

	?>
	<h1 class="text-center py-5">Cart Page </h1>
	<hr>
	<div class="col-lg-10 offset-lg-1">
		<table class="table table-striped table-bordered">
			<thead>
				<tr class="text-center">
					<th>Item:</th>
					<th>Price:</th>
					<th>Quantity:</th>
					<th>Subtotal:</th>
					<th></th>
				</tr>
			</thead>
			<tbody>
				<?php 
				$total = 0;
				if(isset($_SESSION['cart'])){
					foreach($_SESSION['cart'] as $itemId => $quantity){
						$item_query = "SELECT * FROM items WHERE id = $itemId";
						$indiv_item = mysqli_fetch_assoc(mysqli_query($conn, $item_query));
						$subtotal = $indiv_item['price']*$quantity;
						$total += $subtotal;
						?>
						<tr>
							<td>
								<?php echo $indiv_item['name']?>
							</td>
							<td>
								<form action="../Controllers/add-to-cart-process.php" method="POST">
									<input type="hidden" name="name" value="<?php echo $name ?>">
									<input type="hidden" name="fromCartpage" value="fromCartpage">
									<div class="input-group">
											<input type="number" name="quantity" class="form-control" value="<?php echo $quantity ?>">
											<div class="input-group-append">
												<button class="btn btn-sm btn-success" 
												type="submit">+</button>	
											</div>
									</div>
									</form>
								</td>
								<td>
									<?php echo $indiv_item['price']?>
								</td>
								<td>
									<?php echo $quantity ?>
								</td>
								<td>
									<?php echo number_format($subtotal, 2) ?>
								</td>	
							</tr>
							<?php
						}
					}
					?>		
				</tbody>
			</table>
		</div>
		<?php
	}
	?>