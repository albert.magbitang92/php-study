let spanQs = document.querySelectorAll(".spanQ");


spanQs.forEach(indiv_spanQ=>{
	indiv_spanQ.addEventListener("click", ()=>{
		indiv_spanQ.nextElementSibling.classList.remove('d-none');	
		indiv_spanQ.classList.add('d-none');
	});

	let quantity = 0;

	

	indiv_spanQ.nextElementSibling.lastElementChild.addEventListener("change", ()=>{

		quantity = indiv_spanQ.nextElementSibling.lastElementChild.value;

		if(quantity <= 0){
			indiv_spanQ.classList.remove('d-none');
			indiv_spanQ.nextElementSibling.classList.add('d-none');
			alert("Invalid Quantity");			
		}else{
			indiv_spanQ.nextElementSibling.submit();
		}
	})

	indiv_spanQ.nextElementSibling.lastElementChild.addEventListener("keypress", (e)=>
	{
		quantity = indiv_spanQ.nextElementSibling.lastElementChild.value;

		console.log(quantity);
		if(e.keyCode == 13 && quantity <= 0){
			e.preventDefault();
			alert("Invalid Quantity");
			indiv_spanQ.classList.remove('d-none');
			indiv_spanQ.nextElementSibling.classList.add('d-none');
			indiv_spanQ.nextElementSibling.lastElementChild.value = indiv_spanQ.textContent;
		}
	})	
})