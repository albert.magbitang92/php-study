<!DOCTYPE html>
<html>
<head>
  <title><?php get_title(); ?></title>
  <!-- Bootswatch -->
  <link rel="stylesheet" type="text/css" href="https://bootswatch.com/4/superhero/bootstrap.css">
</head>
<body>
  <nav class="navbar navbar-expand-lg navbar-light bg-warning">
   <a class="navbar-brand" href="../index.php"></a>
   <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor03" aria-controls="navbarColor03" aria-expanded="false" aria-label="Toggle navigation">
     <span class="navbar-toggler-icon"></span>
   </button>

   <div class="collapse navbar-collapse" id="navbarColor03">
     <ul class="navbar-nav mr-auto">
       <li class="nav-item active">
         <a class="nav-link" href="../Views/catalog.php">RoadRoller<span class="sr-only">(current)</span></a>
       </li>
       <?php 
       session_start();
       if(isset($_SESSION['user']) && $_SESSION['user']['role_id']==1){
         ?> 
         <li class="nav-item">
          <a class="nav-link" href="add-item.php">Add Item</a>
        </li>
        <?php
      }else{
        ?>
        <li class="nav-item">
          <a class="nav-link" href="cart.php">Cart: <span id="cartCount" class="badge btn-info">
            <?php 
            if (isset($_SESSION['cart'])){
              echo array_sum($_SESSION['cart']);
            }else{
              echo 0;
            }
            ?>
          </span>
        </a>
      </li> 
      <?php
    }
    if(isset($_SESSION['user'])){
      ?>
      <li class="nav-item">
        <a class="nav-link" href="#">Hello, <?php echo $_SESSION['user']['firstName'] ?>!</a>
      </li>
      <li class="nav-item">
       <a class="nav-link" href="../Controllers/logout-process.php">Logout</a>
     </li>
     <?php
   }else{
    ?>
    <li class="nav-item">
      <a class="nav-link" href="login.php">Login</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="register.php">Register</a>
    </li>
    <?php
  }
  ?>

  
</ul>
</div>
</nav>
<!-- Page Contents -->


<?php
get_body_contents()
?>
<!-- Footer -->


<footer class="page-footer font-small bg-dark">
  <div class="footer-copyright text-center py-3">All rights reserved. 2020</div>
</footer>
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>